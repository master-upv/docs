- [ ] Hacks Moodle
	- [ ] Moodle Boostrap
		- [ ] Tableau bootstrap moodle
		- [ ] Accordion bootstrap moodle
		- [ ] Modal boostrap moodle
		- [ ] Callouts bootstrap moodle
- [ ] Tools to explore
	- [ ] https://quarto.org
- [ ] Présentations de base
	- [x] LMS ✅ 2024-07-16
		- [x] Moodle ✅ 2024-07-16
	- [x] Outils auteurs ✅ 2024-07-16
		- [x] Rise360 ✅ 2024-07-16
		- [x] Opale ✅ 2024-07-16
		- [x] Reveal.js ✅ 2024-07-16
- [ ] 