---
title: Introduction aux outils en HNs
draft: false
tags: 
date: 2024-07-17
---

> [!warning] Disclaimer
> Sur cet article ne sont abordées les Humanités Numériques que sous l'angle des outils numériques à disposition des chercheurs. Car cet article fait partie intégrante de la **communauté de pratiques** du master Humanités numériques de l'Université Paul-Valéry Montpellier 3.
> Dans nos cours, les Humanités numériques sont abordées également sous l'angle critique des conséquences du numérique à la fois sur la société et la recherche. Pour mieux connaître notre vision complexe des Humanités numériques, [consulter notre site](https://master-upv.gitpages.huma-num.fr/humanites-numeriques/).

Les **humanités numériques** représentent une intersection dynamique entre les sciences humaines et sociales et les technologies numériques. Elles englobent une variété de méthodes et d'outils qui transforment la recherche, l'enseignement et la diffusion des connaissances. Voici une présentation détaillée des principaux outils utilisés en humanités numériques, organisés par thématiques :

- [Collecte et gestion de données](#outils-de-collecte-et-de-gestion-de-donn%C3%A9es)
- [Analyse textuelle et linguistique](#outils-danalyse-textuelle-et-linguistique)
- [Visualisation et représentation des données](#outils-de-visualisation-et-de-représentation-des-données)
- [Édition et publication numérique](#outils-dédition-et-de-publication-numérique)
- [Collaboration et gestion de projet](#outils-de-collaboration-et-de-gestion-de-projet)
- [Archivage et préservation numérique](#outils-darchivage-et-de-préservation-numérique)


> [!abstract] Article dense, mais non exhaustif !
> Dans la mesure où les humanités numériques n'ont pas de frontière établie avec les autres aspects de la recherche en SHS, et qu'elles infiltrent la totalité du monde académique, il nous fallait rédiger un article représentatif.
> Nous vous glissons des pages d'autres laboratoires en HNs qui ont fait leur propre référencement. 
> - [Université de New York](https://guides.nyu.edu/digital-humanities/tools-and-software)
> - [Massachussets Institute of Technology (MIT)](https://libguides.mit.edu/c.php?g=176357&p=1158575)
> - [Université du Minnesota Duluth](https://libguides.d.umn.edu/DH/dh_tools)
> - [Université de Harvard](https://digitalhumanities.fas.harvard.edu/resources/choosing-digital-methods-and-tools/)
> Les universités sont sont pas les seules à avoir leur liste d'outils :
> - [DH-tech, un collectif de développeurs en HNs](https://dh-tech.github.io/awesome-digital-humanities/)

Il existe des [[ecosystems|écosystèmes]], et en tant que futurs ingénieurs d'étude en Humanités Numériques vous devez les connaître, notamment celui de [Huma-num.fr](https://huma-num.fr).


## Outils de collecte et de gestion de données

La collecte et la gestion efficace des données sont des étapes cruciales dans tout projet de recherche en humanités numériques. Les outils dans cette catégorie permettent aux chercheurs de transformer des sources analogiques en données numériques exploitables et de les organiser de manière structurée.

### Outils de numérisation

- **Scanners et appareils photo haute résolution** : 
  Ces dispositifs sont essentiels pour la numérisation de documents physiques, d'artefacts et d'œuvres d'art. Les scanners à plat sont idéaux pour les documents papier, tandis que les scanners de livres spécialisés permettent de numériser des ouvrages reliés sans les endommager. Les appareils photo haute résolution sont particulièrement utiles pour capturer des objets tridimensionnels ou des documents fragiles.

- **Logiciels de reconnaissance optique de caractères (OCR)** : 
  Des outils comme ABBYY FineReader et Tesseract transforment les images de texte en texte éditable et recherchable. ABBYY FineReader est une solution commerciale puissante offrant une précision élevée et la prise en charge de nombreuses langues. Tesseract, open-source et gratuit, est une alternative robuste, particulièrement utile pour les projets à budget limité ou nécessitant une personnalisation poussée.

- **Transkribus** : 
  Cet outil innovant utilise l'intelligence artificielle pour la transcription automatique de documents manuscrits. Il est particulièrement précieux pour les historiens et les archivistes travaillant sur des sources primaires manuscrites. Transkribus peut être entraîné sur des écritures spécifiques, améliorant ainsi sa précision pour des corpus particuliers.

### Bases de données

- **Bases relationnelles** : 
  MySQL et PostgreSQL sont des systèmes de gestion de bases de données relationnelles (SGBDR) largement utilisés. MySQL est apprécié pour sa facilité d'utilisation et ses performances, tandis que PostgreSQL offre des fonctionnalités avancées pour la gestion de données complexes. Ces bases de données sont idéales pour stocker et interroger des données structurées, comme des catalogues, des métadonnées ou des résultats d'analyse.

- **Bases NoSQL** : 
  MongoDB et CouchDB représentent une approche différente du stockage de données, particulièrement adaptée aux données non structurées ou semi-structurées. MongoDB excelle dans la gestion de grands volumes de données hétérogènes, tandis que CouchDB est apprécié pour sa facilité de réplication et sa synchronisation entre différents appareils. Ces bases de données sont particulièrement utiles pour les projets impliquant des données textuelles complexes ou des structures de données évolutives.

- **Gestion de références bibliographiques** : 
  Zotero et Mendeley sont des outils essentiels pour organiser et citer des sources. Zotero, open-source, offre une grande flexibilité et une intégration poussée avec les navigateurs web. Mendeley, en plus de la gestion bibliographique, propose des fonctionnalités de réseau social académique. Ces outils permettent aux chercheurs de collecter, organiser et partager des références, facilitant ainsi la collaboration et la rédaction de publications scientifiques.

L'utilisation judicieuse de ces outils de collecte et de gestion de données permet aux chercheurs en humanités numériques de constituer des corpus numériques riches et bien organisés, posant ainsi les fondations solides pour des analyses approfondies et des découvertes innovantes.

## Outils d'analyse textuelle et linguistique

L'analyse textuelle et linguistique est au cœur de nombreux projets en humanités numériques. Les outils dans cette catégorie permettent aux chercheurs d'explorer, d'analyser et d'interpréter de vastes corpus textuels avec une précision et une efficacité inégalées.

### Analyse de corpus

- **TXM (Textométrie)** : 
  TXM est une plateforme open-source puissante pour l'analyse statistique de textes. Elle offre une interface graphique conviviale et des fonctionnalités avancées telles que la recherche de motifs complexes, l'analyse factorielle des correspondances et la création de concordances. TXM est particulièrement apprécié pour sa capacité à gérer de grands corpus et à produire des visualisations statistiques sophistiquées, ce qui en fait un outil de choix pour les linguistes et les littéraires.

- **Iramuteq** : 
  Cet outil gratuit, basé sur le langage R, se spécialise dans l'analyse lexicométrique. Il permet de réaliser des analyses multidimensionnelles de textes et de discours, incluant des analyses de similitude, des nuages de mots et des classifications hiérarchiques. Iramuteq est particulièrement utile pour les chercheurs en sciences sociales qui souhaitent explorer les structures lexicales et sémantiques de leurs corpus.

- **AntConc** : 
  Développé par Laurence Anthony, AntConc est un outil gratuit et multiplateforme pour l'analyse de concordance et de corpus. Il offre des fonctionnalités essentielles telles que la recherche de mots-clés en contexte, l'analyse de collocations et la génération de listes de mots. Sa simplicité d'utilisation et sa robustesse en font un outil idéal pour les débutants en analyse de corpus, tout en restant suffisamment puissant pour des analyses plus avancées.

### Traitement automatique du langage naturel (TAL)

- **NLTK (Natural Language Toolkit)** : 
  NLTK est une bibliothèque Python complète pour le traitement du langage naturel. Elle offre une vaste gamme d'outils pour diverses tâches de TAL, incluant la tokenisation, l'étiquetage morphosyntaxique, l'analyse syntaxique, et la classification de textes. NLTK est accompagné d'une documentation extensive et de corpus intégrés, ce qui en fait un excellent choix pour l'enseignement et la recherche en linguistique computationnelle.

- **SpaCy** : 
  SpaCy est une bibliothèque Python moderne conçue pour le traitement du langage naturel en production. Elle se distingue par sa rapidité et son efficacité, particulièrement pour le traitement multilingue. SpaCy offre des modèles pré-entraînés pour de nombreuses langues et excelle dans des tâches telles que la reconnaissance d'entités nommées, l'analyse syntaxique en dépendances et la désambiguïsation sémantique. Son architecture modulaire permet une intégration facile dans des pipelines de traitement complexes.

- **TreeTagger** : 
  Développé par Helmut Schmid, TreeTagger est un outil spécialisé dans l'étiquetage morphosyntaxique et la lemmatisation. Il utilise des arbres de décision pour attribuer des catégories grammaticales aux mots d'un texte avec une grande précision. TreeTagger est particulièrement apprécié pour sa capacité à traiter de nombreuses langues et pour sa facilité d'intégration dans des chaînes de traitement existantes. Il est largement utilisé dans les projets nécessitant une analyse linguistique fine, comme l'analyse stylistique ou la linguistique de corpus.

Ces outils d'analyse textuelle et linguistique offrent aux chercheurs en humanités numériques la possibilité d'explorer des corpus de manière approfondie, de découvrir des motifs linguistiques subtils et de tester des hypothèses à grande échelle. Leur utilisation judicieuse peut mener à des découvertes significatives dans des domaines aussi variés que la littérature, l'histoire, la sociologie et la linguistique, ouvrant ainsi de nouvelles perspectives de recherche et d'interprétation des textes.

## Outils de visualisation et de représentation des données

La visualisation des données joue un rôle crucial dans les humanités numériques, permettant de transformer des informations complexes en représentations visuelles compréhensibles et révélatrices. Ces outils offrent aux chercheurs la possibilité de découvrir des motifs, des tendances et des relations qui pourraient rester invisibles dans les données brutes.

### Cartographie et Systèmes d'Information Géographique (SIG)

- **QGIS (Quantum GIS)** : 
  QGIS est un logiciel SIG open-source puissant et polyvalent. Il permet aux chercheurs de créer, éditer, visualiser et analyser des données géospatiales avec une grande précision. QGIS offre une interface intuitive et une large gamme de plugins, ce qui le rend adapté à diverses applications, de la cartographie historique à l'analyse spatiale complexe. Son interopérabilité avec d'autres formats de données et sa communauté active en font un outil incontournable pour les projets impliquant des données géographiques.

- **Palladio** : 
  Développé par l'Université Stanford, Palladio est un outil web gratuit spécialement conçu pour la visualisation de données géo-temporelles en humanités numériques. Il excelle dans la création de visualisations interactives combinant des données temporelles, spatiales et relationnelles. Palladio est particulièrement utile pour explorer des jeux de données historiques, permettant aux chercheurs de découvrir des modèles et des relations à travers le temps et l'espace. Sa facilité d'utilisation et son absence de nécessité de programmation le rendent accessible aux chercheurs de tous niveaux techniques.

- **Gephi** : 
  Bien que principalement connu pour la visualisation de réseaux, Gephi offre également des fonctionnalités puissantes pour la représentation spatiale des données. Cet outil open-source permet de créer des visualisations de réseaux géolocalisés, combinant ainsi l'analyse de réseaux avec des informations géographiques. Gephi est particulièrement utile pour les projets explorant les relations spatiales entre des entités, comme l'analyse de réseaux sociaux historiques ou l'étude de la diffusion des idées à travers des régions géographiques.

### Visualisation de données

- **Tableau** : 
  Tableau est une plateforme de visualisation de données leader sur le marché, offrant une interface intuitive pour créer des graphiques interactifs sophistiqués. Son point fort réside dans sa capacité à connecter et intégrer diverses sources de données, permettant des analyses croisées complexes. Tableau excelle dans la création de tableaux de bord interactifs, ce qui en fait un outil précieux pour la présentation de résultats de recherche de manière engageante et accessible. Bien que propriétaire, Tableau offre des licences gratuites pour les étudiants et les enseignants.

- **D3.js (Data-Driven Documents)** : 
  D3.js est une bibliothèque JavaScript puissante pour créer des visualisations de données personnalisées et interactives sur le web. Elle offre un contrôle fin sur le rendu final, permettant aux développeurs de créer des visualisations uniques et adaptées à des besoins spécifiques. D3.js est particulièrement apprécié pour sa flexibilité et sa capacité à produire des visualisations dynamiques et réactives. Bien que nécessitant des compétences en programmation, D3.js est inégalé pour les projets nécessitant des visualisations sur mesure et hautement interactives.

- **RAWGraphs** : 
  RAWGraphs se positionne comme un outil de visualisation de données rapide et simple d'utilisation. Basé sur le web et open-source, il permet aux utilisateurs de créer des visualisations sophistiquées sans nécessiter de compétences en programmation. RAWGraphs offre une variété de modèles de visualisation, allant des graphiques classiques aux représentations plus innovantes. Sa force réside dans sa simplicité d'utilisation et sa capacité à produire rapidement des visualisations de qualité, ce qui en fait un excellent outil pour l'exploration initiale des données ou pour les chercheurs ayant besoin de créer des visualisations sans investir beaucoup de temps dans l'apprentissage d'outils complexes.

Ces outils de visualisation et de représentation des données offrent aux chercheurs en humanités numériques la possibilité de donner vie à leurs données, de découvrir de nouvelles perspectives et de communiquer efficacement leurs résultats. Que ce soit pour l'analyse spatiale, la visualisation de réseaux complexes ou la création de graphiques interactifs, ces outils enrichissent considérablement la boîte à outils du chercheur moderne, permettant des analyses plus profondes et des présentations plus impactantes des résultats de recherche.

## Outils d'édition et de publication numérique

L'édition et la publication numériques sont devenues des aspects essentiels de la diffusion des connaissances en humanités numériques. Ces outils permettent aux chercheurs de présenter leurs travaux de manière innovante, interactive et accessible à un large public.

### Plateformes d'édition

- **Omeka** :
  Omeka est une plateforme open-source conçue spécifiquement pour la création d'expositions numériques et de collections en ligne. Elle offre une interface conviviale permettant aux chercheurs et aux institutions culturelles de publier des collections d'objets numériques avec des métadonnées riches. Omeka excelle dans :
  - La création d'expositions thématiques interactives
  - La gestion de collections patrimoniales numériques
  - L'intégration de normes de métadonnées comme Dublin Core
  - La personnalisation via des thèmes et des plugins

  Son architecture flexible en fait un outil de choix pour les projets allant de petites expositions à de vastes archives numériques.

- **WordPress** :
  Bien que principalement connu comme système de gestion de contenu pour les blogs, WordPress s'est imposé comme une plateforme puissante pour les publications académiques grâce à ses nombreux plugins spécialisés. Ses atouts pour les humanités numériques incluent :
  - Une grande flexibilité et personnalisation
  - Des plugins dédiés à la gestion de bibliographies, de citations et de peer-review
  - La possibilité de créer des revues en ligne, des monographies numériques ou des carnets de recherche
  - Une communauté active développant constamment de nouvelles fonctionnalités

  WordPress offre un équilibre entre facilité d'utilisation et puissance, le rendant adapté à une variété de projets de publication académique.

- **OpenEdition** :
  OpenEdition est une infrastructure complète pour l'édition électronique en sciences humaines et sociales. Elle se compose de plusieurs plateformes :
  - OpenEdition Journals : pour la publication de revues scientifiques
  - OpenEdition Books : pour l'édition de livres numériques
  - Hypothèses : pour les carnets de recherche

  OpenEdition se distingue par :
  - Son modèle de publication en libre accès
  - Sa visibilité internationale et son indexation dans les principales bases de données académiques
  - Ses outils de diffusion et de promotion des publications
  - Son respect des standards académiques et éditoriaux

  Cette plateforme est particulièrement adaptée aux chercheurs et institutions souhaitant publier des travaux de haute qualité avec une large diffusion dans le monde francophone et au-delà.

### Outils d'encodage

- **Oxygen XML Editor** :
  Oxygen XML Editor est un environnement de développement intégré (IDE) puissant pour l'édition de documents XML, particulièrement utile pour le travail avec la Text Encoding Initiative (TEI). Ses caractéristiques clés incluent :
  - Un support complet pour XML, XSLT, XQuery et les technologies associées
  - Des outils spécifiques pour l'édition TEI, y compris des modèles et des schémas prédéfinis
  - Des fonctionnalités avancées de validation et de transformation de documents
  - Une interface utilisateur personnalisable et des outils de collaboration

  Oxygen est l'outil de prédilection pour les projets d'édition numérique complexes nécessitant un encodage XML/TEI précis et détaillé.

- **eXist-db** :
  eXist-db est une base de données native XML open-source qui offre des capacités avancées de stockage et d'interrogation de documents XML. Ses points forts pour les humanités numériques sont :
  - Un moteur de recherche plein texte puissant
  - La prise en charge de XQuery pour des requêtes complexes sur les documents XML
  - Des outils intégrés pour le développement d'applications web basées sur XML
  - La possibilité de gérer de grandes collections de documents TEI

  eXist-db est particulièrement utile pour les projets nécessitant une gestion efficace de grandes quantités de données structurées en XML, comme les corpus littéraires encodés en TEI ou les archives numériques.

Ces outils d'édition et de publication numérique offrent aux chercheurs en humanités numériques un large éventail de possibilités pour diffuser leurs travaux. Qu'il s'agisse de créer des expositions interactives, de publier des revues en ligne, ou de gérer des corpus XML complexes, ces plateformes et outils permettent de répondre aux besoins variés de la communauté académique, tout en favorisant l'accessibilité et l'innovation dans la diffusion des connaissances.

## Outils de collaboration et de gestion de projet

Dans le domaine des humanités numériques, la collaboration efficace et la gestion de projet structurée sont essentielles pour mener à bien des recherches complexes et interdisciplinaires. Les outils suivants offrent des solutions puissantes pour faciliter le travail d'équipe et optimiser la gestion des projets de recherche.

### Plateformes collaboratives

- **GitHub et GitLab** :
  Ces plateformes de gestion de versions sont devenues incontournables pour la collaboration sur des projets impliquant du code. Elles offrent :
  - Un contrôle de version robuste permettant de suivre les modifications du code
  - Des fonctionnalités de collaboration comme les pull requests et les issues
  - La possibilité de partager et de documenter le code source des projets
  - Des outils d'intégration continue pour automatiser les tests et le déploiement

  GitHub et GitLab sont particulièrement utiles pour les projets d'humanités numériques impliquant du développement logiciel, comme la création d'outils d'analyse textuelle ou de visualisation de données.
  
> [!NOTE] Ce site est hébergé avec Gitlab pages
> Gitlab et Github permettent la publication de sites statiques[^static].
> Ce site de documentation repose sur Quartz, un moteur de rendu pour les coffres [Obsidian](https://obsidian.md), et le site principal du master est codé en HTML, CSS et JavaScript, ce dernier étant un langage de programmation web "client-side" (qui s'exécute sur le navigateur du visiteur). 
> Les 2 composantes du site sont donc hébergées sur le Gitlab de Huma-num.
> Gitlab et Github peuvent donc être envisagées pour la publication également.

[^static]: Un site est statique lors qu'il ne repose sur aucun langage de programme dit "server-side" (qui s'exécute sur le serveur). 

- **Slack, Microsoft Teams, Discord et Mattermost** :
  Ces plateformes de communication d'équipe transforment la collaboration quotidienne des chercheurs. Chacune offre des fonctionnalités uniques adaptées à différents besoins :

  - **Slack et Microsoft Teams** :
    - Canaux de discussion thématiques pour organiser les conversations
    - Intégrations robustes avec des outils professionnels et académiques
    - Fonctionnalités avancées de recherche dans l'historique des conversations
    - Particulièrement adaptés aux environnements professionnels et académiques formels

  - **Discord** :
    - Initialement conçu pour les communautés de gamers, mais de plus en plus adopté dans le milieu académique
    - Excellentes capacités audio et vidéo, idéales pour les séminaires virtuels et les discussions informelles
    - Serveurs thématiques permettant de créer des communautés de recherche ouvertes
    - Interface conviviale et attrayante pour les jeunes chercheurs

  - **Mattermost** :
    - Solution open-source et auto-hébergée, offrant un contrôle total sur les données
    - Hautement personnalisable et adaptable aux besoins spécifiques des projets
    - Fonctionnalités de sécurité avancées, cruciales pour les projets de recherche sensibles
    - Idéal pour les institutions souhaitant maintenir leurs communications sur leurs propres serveurs

  Toutes ces plateformes proposent :
  - Le partage de fichiers et l'intégration avec d'autres outils
  - Des appels audio et vidéo pour des réunions à distance
  - Des applications mobiles pour une communication en déplacement

  Le choix entre ces outils dépendra souvent des besoins spécifiques du projet, des préférences de l'équipe, et des politiques institutionnelles en matière de sécurité et de confidentialité des données. Par exemple, Discord pourrait être privilégié pour des projets impliquant une large communauté de contributeurs, tandis que Mattermost serait plus adapté pour des projets nécessitant un contrôle strict des données.

Ces plateformes facilitent la communication rapide et efficace entre les membres de l'équipe, essentielle pour maintenir une dynamique de collaboration dans les projets d'humanités numériques, souvent caractérisés par leur nature interdisciplinaire et collaborative.

- **Trello et Asana** :
  Ces outils de gestion de tâches visuels aident à structurer et suivre l'avancement des projets. Ils offrent :
  - Des tableaux kanban pour visualiser le flux de travail
  - La possibilité d'assigner des tâches et de définir des échéances
  - Des fonctionnalités de commentaires et de pièces jointes pour chaque tâche
  - Des intégrations avec d'autres outils pour automatiser certains processus

  Trello et Asana sont particulièrement utiles pour gérer les différentes phases d'un projet de recherche en humanités numériques, de la collecte de données à la publication des résultats.

### Environnements de recherche virtuels

- **Jupyter Notebook** :
  Cet environnement interactif révolutionne la façon dont les chercheurs partagent et collaborent sur du code et des analyses. Jupyter Notebook offre :
  - La possibilité de combiner du code exécutable, des visualisations et du texte explicatif
  - Le support de multiples langages de programmation (Python, R, Julia, etc.)
  - Des fonctionnalités de partage et de collaboration en temps réel
  - L'intégration avec des plateformes comme GitHub pour le versionnage

  Jupyter Notebook est particulièrement apprécié dans les projets d'humanités numériques pour le partage de méthodologies d'analyse de données, la création de visualisations interactives et la documentation de processus de recherche.

- **RStudio** :
  Cet environnement de développement intégré (IDE) pour R est devenu un standard pour l'analyse statistique collaborative. RStudio propose :
  - Une interface conviviale pour l'écriture et l'exécution de code R
  - Des outils puissants pour la visualisation de données et la création de rapports
  - La possibilité de créer des documents dynamiques avec R Markdown
  - Des fonctionnalités de collaboration via RStudio Server ou RStudio Cloud

  RStudio est particulièrement utile dans les projets d'humanités numériques impliquant des analyses statistiques complexes, comme l'analyse de corpus textuels ou l'étude de réseaux sociaux historiques.

Ces outils de collaboration et de gestion de projet offrent aux chercheurs en humanités numériques les moyens de travailler efficacement en équipe, de partager leurs connaissances et leurs méthodologies, et de gérer des projets complexes de manière structurée. En combinant ces différents outils, les équipes de recherche peuvent créer des environnements de travail virtuels complets, adaptés aux besoins spécifiques de leurs projets, favorisant ainsi l'innovation et la rigueur scientifique dans le domaine des humanités numériques.

## Outils d'archivage et de préservation numérique

- **Dépôts de données** :
  - **Zenodo** : Pour le partage de données de recherche.
  - **Dataverse** : Pour la gestion et le partage de données.
  - **Huma-Num** : Pour l'infrastructure des humanités numériques en France.

- **Outils de pérennisation** :
  - **LOCKSS** : Pour la préservation distribuée.
  - **Archivematica** : Pour la création de paquets d'archives.

Ces outils sont essentiels pour les chercheurs en humanités numériques, car ils permettent de collecter, analyser, visualiser et diffuser des données de manière innovante et efficace. Le choix des outils dépend souvent des besoins spécifiques du projet, des compétences de l'équipe et des objectifs de recherche. En outre, le domaine des humanités numériques étant en constante évolution, de nouveaux outils et méthodes émergent régulièrement, offrant des possibilités toujours plus avancées et adaptées aux besoins des chercheurs.

Citations:
1. https://fr.wikipedia.org/wiki/Humanit%C3%A9s_num%C3%A9riques
2. https://chnsh.hypotheses.org/367
3. https://www.cairn.info/revue-du-crieur-2017-2-page-144.htm
4. https://lem-umr8584.cnrs.fr/?lang=fr
5. http://www.ens.psl.eu/actualites/acteurs-et-activites-en-humanites-numeriques-l-ens
6. https://www.huma-num.fr
7. https://www.cairn.info/revue-les-cahiers-du-numerique-2017-3-page-91.htm
8. https://odhn.ens.psl.eu/article/que-sont-les-humanites-numeriques
9. https://eduscol.education.fr/456/les-humanites-numeriques-un-enjeu-de-transdisciplinarite
10. https://sciencespo.libguides.com/c.php?g=233287&p=4781528
11. https://uqam-ca.libguides.com/humanites-numeriques/extraire
---
*Rédigé avec l'aide [Perplexity.ai](https://perplexity.ai), prompté par Axelle Abbadie.*