---
title: Écosystèmes d'outils
draft: false
tags: 
date: 2024-07-17
---
Les humanités numériques représentent un domaine en pleine expansion, où les outils et méthodes numériques s'allient aux sciences humaines et sociales pour ouvrir de nouvelles perspectives de recherche. Au cœur de cette évolution, on trouve des infrastructures et des écosystèmes dédiés, dont Huma-Num est un exemple phare en France. Voici une présentation d'Huma-Num et d'autres écosystèmes similaires à l'international :
## Huma-Num

Huma-Num est une très grande infrastructure de recherche (TGIR) française visant à faciliter le tournant numérique de la recherche en sciences humaines et sociales. Ses principales caractéristiques sont :

- Un dispositif technologique permettant d'accompagner les différentes étapes du cycle de vie des données numériques
- Une offre de services variés pour la gestion, le traitement, la diffusion et la préservation des données de recherche
- Un accompagnement des chercheurs dans la gestion et la valorisation de leurs données numériques
- Une grille de services incluant stockage, traitement et diffusion des données

Huma-Num se distingue par son approche intégrée et son focus sur l'infrastructure nationale française, tout en s'inscrivant dans des réseaux internationaux.

Voici une brève présentation des outils proposés au sein de Huma-num.
### Dépôts de données

- **Nakala** :  
    Nakala est une plateforme de dépôt et de partage de données de recherche en sciences humaines et sociales. Elle permet aux chercheurs de stocker, documenter et diffuser leurs données tout en garantissant leur pérennité. Nakala offre :
    
    - Un espace de stockage sécurisé pour les données
    - La possibilité d'attribuer des DOI pour faciliter la citation des données
    - Des outils de description et de documentation des jeux de données
    
- **Nakala Collections** :  
    Une extension de Nakala qui permet de créer des collections de données organisées et interconnectées. Cet outil est particulièrement utile pour les projets impliquant de grandes quantités de données hétérogènes.

### Outils de traitement et d'analyse

- **ISIDORE** :  
    ISIDORE est un moteur de recherche spécialisé dans les données en sciences humaines et sociales. Il permet d'accéder à des millions de documents et de données provenant de sources variées. ISIDORE offre :
    
    - Une interface de recherche simple et intuitive
    - Des filtres avancés pour affiner les résultats
    - L'intégration avec d'autres outils de Huma-Num pour une exploitation optimale des données trouvées
    

### Outils de diffusion

- **Nakala** :  
    En plus de ses fonctionnalités de dépôt, Nakala permet également la diffusion des données de recherche. Les chercheurs peuvent partager leurs données avec la communauté scientifique et le grand public, tout en contrôlant les conditions d'accès et de réutilisation.
- **Huma-Num Box** :  
    Un service de stockage et de partage de fichiers volumineux. Il est particulièrement utile pour les projets nécessitant l'échange de grandes quantités de données entre collaborateurs.

### Outils de préservation

- **Huma-Num Grid** :  
    Une infrastructure de calcul et de stockage distribuée qui permet de traiter et de stocker de grandes quantités de données. Huma-Num Grid est conçu pour assurer la pérennité des données de recherche en les répliquant sur plusieurs sites.
- **Archivematica** :  
    En partenariat avec Huma-Num, Archivematica est utilisé pour la création et la gestion de paquets d'archives numériques conformes aux standards internationaux. Il assure la préservation à long terme des données de recherche.

### Outils collaboratifs

- **Humanités Numériques Collaboratives (HNC)** :  
    Une plateforme qui regroupe plusieurs outils collaboratifs pour les chercheurs en humanités numériques. Elle inclut des services de gestion de projets, de communication et de partage de ressources.

### Services de formation et d'accompagnement

- **Formations et ateliers** :  
    Huma-Num propose régulièrement des formations et des ateliers pour aider les chercheurs à maîtriser les outils numériques et à adopter de bonnes pratiques en matière de gestion des données.
- **Accompagnement personnalisé** :  
    Les équipes de Huma-Num offrent un soutien personnalisé aux chercheurs pour les aider à définir leurs besoins, à choisir les outils adaptés et à mettre en place des solutions efficaces pour leurs projets de recherche.

## Autres écosystèmes internationaux

### DARIAH (Digital Research Infrastructure for the Arts and Humanities)

DARIAH est une infrastructure européenne qui :
- Soutient la recherche numérique dans les arts et les sciences humaines
- Offre des outils, des services et des ressources pour les chercheurs à travers l'Europe
- Favorise la collaboration internationale et l'échange de bonnes pratiques

### CLARIN (Common Language Resources and Technology Infrastructure)

CLARIN est une infrastructure européenne spécialisée qui :
- Se concentre sur les ressources linguistiques et les technologies du langage
- Fournit des outils et des ressources pour l'analyse textuelle et linguistique
- Facilite l'accès à des corpus linguistiques et à des outils d'analyse pour les chercheurs

### Humanities Commons

Développé par la Modern Language Association (MLA), Humanities Commons :
- Est une plateforme en ligne pour le partage de travaux et la collaboration
- Permet aux chercheurs de créer des réseaux professionnels
- Offre des espaces de discussion et de partage de ressources

### Digital Humanities at Oxford

L'Université d'Oxford propose un écosystème complet qui inclut :
- Des formations en humanités numériques
- Des ressources et des outils pour la recherche numérique
- Un réseau de chercheurs et de projets en humanités numériques

### Alliance of Digital Humanities Organizations (ADHO)

Bien que ce ne soit pas un écosystème technique à proprement parler, ADHO :
- Regroupe plusieurs associations internationales en humanités numériques
- Organise des conférences et publie des revues dans le domaine
- Soutient diverses initiatives et projets en humanités numériques

### Consortium on Digital Humanities Software (CODHS)

Cette initiative se concentre sur :
- Le développement et la maintenance de logiciels open-source pour les humanités numériques
- La promotion de standards et de bonnes pratiques dans le développement d'outils pour les humanités numériques

Chacun de ces écosystèmes apporte sa contribution unique au domaine des humanités numériques, reflétant souvent les priorités et les traditions de recherche de leurs contextes géographiques et institutionnels respectifs. Ensemble, ils forment un réseau global d'infrastructures et de ressources qui soutient l'évolution des pratiques de recherche en sciences humaines et sociales à l'ère numérique.

La diversité de ces écosystèmes témoigne de la richesse et de la complexité du champ des humanités numériques, offrant aux chercheurs une variété d'outils, de ressources et de communautés pour mener à bien leurs projets de recherche innovants.

---
*Rédigé avec l'aide [Perplexity.ai](https://perplexity.ai), prompté par Axelle Abbadie.*