---
title: Composez avec Bootstrap !
draft: false
tags: 
date: 2024-07-19
aliases:
---
Pour composer sur Moodle avec Bootstrap, quelques étapes de préparation, ensuite on compose, enfin on ajuste par l'action de "debug".

## 1. Préparation
D'abord, on vérifie la version de Bootstrap sur le Moodle où vous travaillez.
Pour cela, vous pouvez vous aider de l'extension de navigateur "[Wappalyzer](https://www.wappalyzer.com)".

Sur notre Moodle, voici que ce que Wappalyzer affiche.
![[wappalyzer_bootstrap4.6.2.png]]
À partir de ce numéro de version, vous pouvez vous rendre sur la documentation de la bonne version.

> [!warning] La documentation Bootstrap
> Elle est **essentielle**. Quand on n'a pas encore la compétence `développement web`, on pense qu'une documentation est une aide dont il faut apprendre à se passer. Alors qu'un développeur travaille toujours avec la documentation ouverte.
> Vous pouvez prendre des réflexes lorsque vous travaillez toujours sur la même version, mais travailler à partir de la documentation sera toujours **plus rapide** et surtout, **plus fiable**.

Voici [un lien vers la documentation Bootstrap de la version 4.6.x](https://getbootstrap.com/docs/4.6/components/alerts/).

## 2. Schéma
Un bon schéma est toujours une bonne base pour composer.
Prenez une bonne feuille, et schématisez le résultat que vous voulez obtenir.

## 3. Collection
La phase de collection correspond à sélectionner sur la documentation tous les codes dont vous avez besoin, avant de les ajuster.

## 4. Conception
Avant d'imbriquer vos codes, attribuez tous les ID uniques nécessaires.
Et testez chaque éléments/composant seul.

## 5. Imbrication
Si vous travaillez sur une composition imbriquée, imbriquez vos composants à cette étape.

## 6. Debug
Vous aurez des bugs, les premières fois, mais aussi les suivantes.

## 7. Dernière carte
L'IA générative est une excellente alliée pour l'apprentissage du code, ou son ajustement.
Mon conseil est toujours d'essayer en autonomie, et utiliser l'IA en dernier recours, pour apprendre et debug.
Voici une [IA générative pré-promptée](https://www.perplexity.ai/search/aide-moi-a-debug-ma-compositio-kQbTVUTKQvOxBvNeqTlgFw#0) pour vous y aider. 