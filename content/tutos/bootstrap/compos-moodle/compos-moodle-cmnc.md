---
title: Cultures et médiations numériques culturelles
draft: false
tags: 
date: 2024-07-19
aliases:
---
Dans cet espace, il y a plusieurs compositions Bootstrap.

## Image marquée
### Résultat
Une image, avec trois marqueurs, quand on clique sur un marqueur, un tooltip s'affiche.
![[cmnc-img_marked.png]]
### Code
```html
<div class="container position-relative" style="width: fit-content">
    <img class="img-fluid" src="https://i.ibb.co/47VhNqY/CMNC-cover.png" alt="">
    <a tabindex="0" class="position-absolute btn rounded-circle btn-light" style="left: 40%; bottom: 3%; width: 3em; height: 3em; transform: translate(-50%, -50%); box-shadow: #c0d9ff 0px 0px 10px;" role="button" data-toggle="popover" data-trigger="focus" data-placement="left" title="" data-content="Le cours est ouvert du 06/11 au 24/11" data-original-title="Dates d'ouverture">
        <i class="fa fa-calendar-check-o m-auto d-flex justify-content-center align-items-center" aria-hidden="true" style="width: 100%; height: 100%;"></i>
    </a>
    <a tabindex="0" class="position-absolute btn rounded-circle btn-light" style="left: 50%; bottom: 3%; width: 3em; height: 3em; transform: translate(-50%, -50%); box-shadow: #c0d9ff 0px 0px 10px;" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" title="" data-content="Maître de conférence en linguistique et médiations numériques" data-original-title="Lucie Alidières">
        <i class="fa fa-user m-auto d-flex justify-content-center align-items-center" aria-hidden="true" style="width: 100%; height: 100%;"></i>
    </a>
    <a tabindex="0" class="position-absolute btn rounded-circle btn-light" style="left: 60%;bottom: 3%;width: 3em;height: 3em;transform: translate(-50%, -50%);box-shadow: #c0d9ff 0px 0px 10px;" role="button" data-toggle="popover" data-trigger="focus" data-placement="right" title="" data-content="lucie.alidieres@univ-montp3.fr" data-original-title="Contacter l'enseignante">
        <i class="fa fa-envelope m-auto d-flex justify-content-center align-items-center" aria-hidden="true" style="width: 100%; height: 100%;"></i>
    </a>
</div>
```

### Explications
Sous ses airs très simples, cette composition est d'une certaine complexité.
Dans un bloc (`<div>`), auquel on donne les classes `container position-relative` et la propriété `style="width: fit-content"`, on compose avec une image, des marqueurs et des tooltips.
Le bloc et son image pour structurer : 
```html
<div class="container position-relative" style="width: fit-content">
    <img class="img-fluid" src="https://i.ibb.co/47VhNqY/CMNC-cover.png" alt="">
	ICI LES MARQUEURS
</div>
```

Chaque marqueur est composé d'un lien `<a>` dont voici les propriétés : 
- classes `position-absolute btn rounded-circle btn-light` 
- `role="button"` qui permet de l'utiliser comme un bouton et non un lien
- `data-toggle="popover"` déclenche un "popover", ce qui correspond au tooltip, la micro fenêtre qui s'affiche quand on clique.
- `data-trigger="focus"` qui permet de garder ce popover ouvert jusqu'à cliquer ailleurs.
- `data-placement="left"` qui définit que le popover s'ouvre à gauche du clic, dans la composition on a 3 popovers, qui s'ouvrent respectivement à gauche (`left`), au-dessus (`top`) et à droite (`right`). *Cette consigne n'est respectée que si la place nécessaire sur la page est disponible.*
- `title` peut rester vide
- `data-original-title` qui correspond au titre du popover.
- `data-content` qui contient le message du popover.
- dans l'attribut `style=""`, on voit plusieurs valeurs.
	- `left: 40%` et `bottom: 3%` permettent de positionner le marqueur sur l'image.
	- `width: 3em` et `height: 3em` définissent la taille du bouton, ici on veut qu'il fasse un diamètre de 3 fois la taille du texte.
	- `transform: translate(-50%, -50%);` permet de positionner le contenu du lien en plein centre du bouton de 3em de diamètre.
	- `box-shadow: #c0d9ff 0px 0px 10px;`, dans l'ordre, donne une ombre portée de couleur <span style="color:#c0d9ff">◉ #c0d9ff</span> éloignée à 0px sur l'axe x, 0px sur l'axe y, avec un flou de 10px. L'utilisation de cette ombre permet de créer un halo de lumière.
À l'intérieur de ce lien, entre `<a>` et `</a>`, on met le texte du lien, ici, on l'a remplacé par une icône Fontawesome[^fontawesome].
- Les classes `fa fa-calendar-check-o` appartiennent à Fontawesome, pour désigner l'icône qu'on veut.
- `m-auto` donne des marges automatiques, `justify-content-center` et `align-items-center` sont des méthodes de centrage vertical et horizontal dans l'utilisation des flexbox.
```html
    <a tabindex="0" class="position-absolute btn rounded-circle btn-light" style="left: 40%; bottom: 3%; width: 3em; height: 3em; transform: translate(-50%, -50%); box-shadow: #c0d9ff 0px 0px 10px;" role="button" data-toggle="popover" data-trigger="focus" data-placement="left" title="" data-content="Le cours est ouvert du 06/11 au 24/11" data-original-title="Dates d'ouverture">
        <i class="fa fa-calendar-check-o m-auto d-flex justify-content-center align-items-center" aria-hidden="true" style="width: 100%; height: 100%;"></i>
    </a>
```

#### Bac à sable
<iframe height="460" style="width: 100%;" scrolling="no" title="Image avec marqueurs" src="https://codepen.io/master-hn/embed/dyBMGXY?default-tab=result&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true"/>

--- 

[^fontawesome]: Notre version de Moodle embarque [Fontawesome 4.7.0](https://fontawesome.com/v4/icons/)