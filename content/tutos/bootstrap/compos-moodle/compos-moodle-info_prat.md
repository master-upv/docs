---
title: Infos et pratiques du master - Production numérique
draft: false
tags: 
date: 2024-07-19
aliases:
---
Dans cet espace, il y a plusieurs compositions Bootstrap.
### Bannière / alert
#### Résultat
![[info_prat-prod_num-callout.png]]
#### Code
```html
<div class="alert alert-success text-center">
    <h4 class="alert-heading">Vous entrez dans une immersion professionnelle !</h4>
    <hr>
    <p>Pour la première fois cette année, une production commune aux M1 et M2.</p>
    <p>2 projets, 2 promos, 3 devoirs.</p>
</div>
```
#### Explication
Il existe plusieurs bannières (alerts), voici [une liste](https://getbootstrap.com/docs/4.6/components/alerts/).
Ici nous avons utilisé le code du callout success `alert alert-success`, avec un titre de bannière `alert-heading` et un séparateur `<hr>`.

---
### Carte avec modal
Pour présenter les partenaires, nous avons imbriqué trois composants, la carte, le bouton et la modal[^modal].
#### Résultat
![[info_prat-prod_num-card_modal.png]]Si on clique sur "Découvrir leur projet", une modal[^modal] s'ouvre :
![[info_prat-prod_num-card_modal2.png]]
[^modal]: Une modale est une page qui s'ouvre à l'intérieur de la page. On les appelle aussi pop-in (*en opposition aux pop-ups qui sont des pages qui s'ouvre à l'extérieur de la page*).
#### Code
D'abord, le code de la carte, qu'on met avant le code de la modal.
```html
<div class="card shadow-sm mw-75 h-100">
                <div class="card-body">
                    <h5 class="card-title" style="color: #8A8DC3;">Archives Départementales de l'Hérault</h5>
                    <h5 class="card-subtitle text-muted">Pierres Vives</h5>
                    <hr>
                    <p class="card-text">Centre de conservation et de diffusion des documents historiques du département de l'Hérault.</p>
                    <p class="card-text">Les archives abritent une vaste collection de documents historiques accessibles au public pour la recherche et la préservation du patrimoine.</p>
                    <a data-target="#ProjetModal1" data-toggle="modal" class="btn" href="#" style="background-color: #8A8DC3; color:white">Découvrir leur projet</a>
                </div>
```
Puis, plus loin, le code de la modal : 
```html
<div id="ProjetModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ProjetModal1Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ProjetModal1Label">Projet des Archives Départementales</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h4 class="text-center">La retirada</h4>
                <p>Le service des archives départementales de l’Hérault fait appel à la formation du master humanités numériques afin de créer une ressource sur le thème de la retirada.</p>
                <p>A partir d’un corpus de témoignages archivés, les étudiants sont invités à construire un discours de médiation avec l’outil numérique.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>
```
#### Explications
##### La carte
Une carte est toujours dans la même structure : 
```html
<div class="card">
  <div class="card-body">
    Ici le texte
  </div>
</div>
```
À cette structure, nous avons ajouté une `card-heading` sur une balise titre de niveau 5 `<h5>`, et une couleur personnalisée <span style="color:#8A8DC3">◉ #8A8DC3</span>.
```html
<h5 class="card-title" style="color: #8A8DC3;">Archives Départementales de l'Hérault</h5>
```
Et un lien `<a>` dont la cible `#ProjectModal1` correspond ID de la modal qu'on veut ouvrir (ref code modal : `<div id="ProjetModal1">`). On donne la classe `btn` à ce lien pour qu'il prenne la forme d'un bouton, et on attribue une couleur de fond (`background-color: #8A8DC3`) et une couleur d'écriture (`color: white`). Le texte entre le `>`de fin de la balise `<a>` et le `</a>` est le texte du bouton, ici ce texte est rédigé comme une [microcopie](https://lagrandeourse.design/blog/ux-writing/quest-ce-quune-microcopie-et-comment-la-reussir/).
```html
<a data-target="#ProjetModal1" data-toggle="modal" class="btn" href="#" style="background-color: #8A8DC3; color:white">Découvrir leur projet</a>
```
##### La modal
Les modals sont probablement les composants Bootstrap les plus difficiles à mettre en place avec les accordéons.
Il faut donc un peu de patience les premières fois.
Une modal Bootstrap suit toujours la même structure : 
- Le conteneur de la modal.
	- Le document "dialog" qui contient tous les éléments de la modal.
		- Le contenu de la modal, là où vous pouvez écrire.
			- L'en-tête de la modal, avec son titre et son premier bouton de fermeture.
			- Le corps de la modal, avec votre texte, vos images, ou vos compositions internes.
			- Le pied de la modal, avec les boutons d'action, et un deuxième bouton de fermeture.
Le conteneur qui contient l'ID, que vous devez répéter dans l'attribut `aria-labelledby` et toutes les autres mentions qui sont obligatoires que ça fonctionne correctement : 
```html
<div id="ID-DE-VOTRE-MODAL" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ID-DE-VOTRE-MODAL" aria-hidden="true">

Ici le reste de votre modal

</div>
```
Ensuite l'enchaînement document et contenu qui est toujours la même : 
```html
<div id="ID-DE-VOTRE-MODAL" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ID-DE-VOTRE-MODAL" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		
			Ici le reste de votre modal
		
		</div>
	</div>
</div>
```
À l'intérieur de cette structure, vous devez disposer l'en-tête, le corps et le pied.
Notez : 
- En-tête :
	- Le titre a un ID LABEL- et votre ID de modal.
	- Vous n'avez pas besoin de modifier le bouton.
- Corps :
	- Vous pouvez mettre absolument tout ce que vous voulez, nous l'utilisons souvent pour des intégrations (les `<iframe>` donnés par Genial.ly par exemple, ou Youtube).
- Pied :
	- Ici il n'y a qu'un bouton pour fermer, mais vous pouvez modifier le bouton pour en faire un lien qui pointe vers un autre site, ou une autre modal.

```html
<div id="ID-DE-VOTRE-MODAL" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ID-DE-VOTRE-MODAL" aria-hidden="true">

	<div class="modal-dialog" role="document">
		<div class="modal-content">
<!-- Début en-tête -->
            <div class="modal-header">
                <h5 class="modal-title" id="LABEL-ID-DE-VOTRE-MODAL">ICI ÉCRIRE VOTRE TITRE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
<!-- Fin en-tête et début corps -->

            <div class="modal-body">
                Ici vous écrivez le corps.
            </div>
<!-- Fin corps et début pied -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
		</div>
	</div>
</div>
```

#### Bac à sable
Nous vous glissons dans un bac à sable CodePen le code de cette composition, pour vous permettre d'essayer d'en modifier le contenu.
<iframe height="460" style="width: 100%;" scrolling="no" title="Carte avec Modal" src="https://codepen.io/master-hn/embed/qBzZEmd?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true"></iframe>

---
### Bloc de citation
#### Résultat
![[info_prat-prod_num-quote_block.png]]
#### Code
```html
<div class="card">
    <div class="card-header">
        Définition "Étude de cas"
    </div>
    <div class="card-body">
        <blockquote>
            <p>Proposition, à un petit groupe, d’un problème réel ou fictif en vue de poser un diagnostic, de proposer des solutions et de déduire des règles ou des principes applicables à des cas similaires.</p>
            <footer class="blockquote-footer"><cite title="Source Title">Chamberland, Lavoie et Marquis, 1995</cite></footer>
        </blockquote>
    </div>
</div>
```
#### Explication
Comme vous le voyez, il s'agit d'une carte, avec un `<blockquote>` à l'intérieur.
La balise `<footer>`avec la classe `blockquote-footer` permet de mettre la source (avec `<cite>`) ou un commentaire.

---
### Accordéon
#### Résultat
Lorsqu'on clique sur un élément de l'accordéon, il s'ouvre et celui qui était ouvert se ferme.
![[info_prat-prod_num-accordion.png]]
#### Code
Pour simplifier la lecture du code, nous ne mettons que 2 éléments à cet accordéon.
```html
<div id="accordion" class="shadow-sm">
    <div class="card mb-0">
        <div class="card-header" id="heading1" style="background-color: #8A8DC3; color:white">
            <h5 class="mb-0">
                <button class="btn btn-link text-white" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                    Titre du premier élément
                </button>
            </h5>
        </div>

        <div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#accordion">
            <div class="card-body">
                Ici le contenu de votre premier élément.
            </div>
        </div>
    </div>
    <div class="card mb-0">
        <div class="card-header" id="heading2" style="background-color: #8A8DC3; color:white">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed text-white" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                    Titre du deuxième élément
                </button>
            </h5>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
            <div class="card-body">
                Ici le contenu de votre deuxième élément.
            </div>
        </div>
    </div>
</div>
```
#### Explication
La structure : 
- Conteneur de l'accordéon
	- Carte qui contient un élément
		- Le bouton d'un élément
		- Le contenu d'un élément
	- Autres cartes
		- Même structure pour chaque carte
Le conteneur de l'accordéon doit avoir un ID unique.
```html
<div id="ID-DE-LACCORDEON">
	Ici les cartes.
</div>
```
À l'intérieur de cet accordéon, on met des cartes, commençons par une.
```html
<div id="ID-DE-LACCORDEON">
    <div class="card mb-0">
		Ici on mettra le bouton d'ouverture et le contenu qui s'affiche.
    </div>
</div>
```
Et à l'intérieur de cette carte, on met le bouton et le contenu.
Le bouton : 
- Dans un bloc (`<div>`) avec la classe `card-header`, on donne un ID unique qu'on pourra appeler dans le bloc de contenu avec l'attribut `labelledby`.
	- On imbrique un titre, ici `<h5>` dans lequel on imbrique également un bouton (`<a class="btn">`) avec les attributs `data-target="#{L'ID unique du contenu qui suit}"`, `aria-controls="{L'ID unique du contenu qui suit}`, `data-toggle="collapse"` et `aria-expanded="true"`.
	  *Ça a l'air un peu redondant entre les attributs de base et les aria, c'est pour assurer la compatibilité de plusieurs navigateurs*.

Ensuite, on met le contenu :
- Un bloc (`<div>`) avec la classe `collapse`, si on veut que ce contenu soit déjà visible à l'affichage, on ajoute la classe `show`. Ensuite, quelques attributs utiles : `aria-labelledby="{L'ID unique attribué au card-header}"` et `data-parent="#ID-DE-LACCORDEON"`
```html
<div id="ID-DE-LACCORDEON">
    <div class="card mb-0">
        <div class="card-header" id="heading1" style="background-color: #8A8DC3; color:white">
            <h5 class="mb-0">
                <button class="btn btn-link text-white" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                    Titre du premier élément
                </button>
            </h5>
        </div>

        <div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#ID-DE-LACCORDEON">
            <div class="card-body">
                Ici le contenu de votre premier élément.
            </div>
        </div>
    </div>
</div>
```
#### Bac à sable
Nous vous glissons dans un bac à sable CodePen le code de cette composition, pour vous permettre d'essayer d'en modifier le contenu.

<iframe height="460" style="width: 100%;" scrolling="no" title="Accordéon exemple" src="https://codepen.io/master-hn/embed/gONrbEv?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true"></iframe>