---
title: Bootstrap dans notre master
draft: false
tags: 
date: 2024-07-19
aliases:
---
Sur cette page, nous vous présentons les compositions présentes sur nos cours Moodle, pour permettre une réutilisation ou une modification.

Cette page est structurée cours par cours.


> [!important] Infos pratiques
> Ces compositions ont été réalisée sur Moodle, avec **Bootstrap 4.6.2**, dont voici [la documentation](https://getbootstrap.com/docs/4.6/components/alerts/).
> Pour faire vos propres compositions Boostrap, [[compos-custom|c'est par ici]].
> **Certaines explications sont munies d'un bac à sable pour tester.**

--- 
## Nos cours
Une liste des espaces de cours, avec leurs compositions expliquées pas-à-pas.
- Informations et pratiques du master - [[compos-moodle-info_prat|voir les compositions]]
- Culture médiations culturelles et numériques - [[compos-moodle-cmnc|voir les compositions]]