---
title: Digital Garden du Master HumaNum
draft: false
tags: 
date: 2024-03-08
---
Bienvenue sur la page d'accueil de notre communauté de pratiques dédiée aux Humanités numériques !

Notre communauté a pour objectif de **partager des pratiques** et d'inciter chacun à **documenter ses processus** et ses pratiques en tant qu'ingénieurs pédagogiques, médiateurs numériques ou ingénieurs d'études en humanités numériques. 

### À qui s'adresse cette communauté ?

Cette plateforme est principalement destinée aux **étudiants** et aux **membres de l'équipe pédagogique** du master Humanités numériques de l'Université Paul-Valéry Montpellier 3. Cependant, son contenu est ouvert à toute personne curieuse et désireuse d'approfondir ses connaissances dans ce domaine.

### Que trouverez-vous ici ?

Dans notre digital garden, vous découvrirez :

- **Des présentations d'outils** et de méthodes
- **Des tutoriels** pratiques pour vous aider dans vos projets

### Domaines d'intérêt

Nous aborderons principalement les grands domaines suivants :

- E-learning
- Ingénierie des savoirs
- Humanités numériques
- Ingénierie numérique

### Comment contribuer ?

Pour contribuer à cette communauté, nous vous invitons à contacter notre équipe à l'adresse suivante : **ingenierie.masterhn@univ-montp3.fr**. Notez que **vous devez être inscrit au sein du master pour participer**.

### Soutien à la formation

Cette communauté vient en soutien et en approfondissement du déroulement de la formation ainsi que des immersions professionnelles. 

### Liens utiles

Pour plus d'informations, vous pouvez consulter les ressources suivantes :

- [Université Paul-Valéry Montpellier 3](https://www.univ-montp3.fr)
- [ITIC](https://itic.www.univ-montp3.fr)
- [Site du master Humanités numériques](https://master-upv.gitpages.huma-num.fr)

Nous vous souhaitons la bienvenue et espérons que vous trouverez ici des ressources enrichissantes pour votre parcours en Humanités numériques !