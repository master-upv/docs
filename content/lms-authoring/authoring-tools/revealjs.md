---
title: Reveal.js
draft: false
tags:
  - outils-auteurs
date: 2024-07-16
---
## Présentation de reveal.js

Reveal.js est une bibliothèque JavaScript open-source qui permet de créer des présentations interactives en utilisant HTML, CSS et JavaScript. Développée par Hakim El Hattab, cette bibliothèque est largement utilisée pour créer des diaporamas élégants et dynamiques, comparables à ceux réalisés avec PowerPoint ou Google Slides, mais avec la flexibilité et la puissance du web.

> [!tip] Astuce
> Reveal.js vous permet de proposer le même contenu selon différentes vues, de les héberger sur Github ou Gitlab gratuitement, et de les intégrer dans n'importe quel LMS.

### Installation et utilisation de base

Pour commencer à utiliser reveal.js, vous pouvez l'installer via npm, pnpm ou yarn, ou télécharger directement le zip depuis GitHub.

```bash
npm install reveal.js
# ou
pnpm add reveal.js
# ou
yarn add reveal.js
```

Ensuite, créez une page HTML avec la structure suivante :

```html
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="node_modules/reveal.js/dist/reveal.css">
  <link rel="stylesheet" href="node_modules/reveal.js/dist/theme/white.css">
</head>
<body>
  <div class="reveal">
    <div class="slides">
      <section>Slide 1</section>
      <section>Slide 2</section>
    </div>
  </div>
  <script src="node_modules/reveal.js/dist/reveal.js"></script>
  <script>
    Reveal.initialize();
  </script>
</body>
</html>
```

### Fonctionnalités principales

- **Navigation par sections :** Les présentations sont organisées en sections, chaque balise `<section>` représentant une diapositive. Il est possible de créer des sous-sections pour une navigation plus complexe.
- **Thèmes personnalisables :** Reveal.js propose plusieurs thèmes préconçus, mais vous pouvez également créer vos propres thèmes en CSS.
- **Support Markdown :** Grâce à un plugin, vous pouvez écrire vos diapositives en Markdown, ce qui simplifie la création de contenu.
- **Syntaxe LaTeX :** Pour les présentations scientifiques, reveal.js supporte l'inclusion de formules LaTeX.
- **Export PDF :** Les présentations peuvent être exportées en PDF pour une distribution plus facile.
### Différentes vues
Comme [[opale|Opale]], il est possible d'obtenir plusieurs vues de votre contenu.

1. **Vue Scroll :**
   *[Voir un exemple](https://revealjs.com/demo/?view=scroll)*
   La vue scroll permet de naviguer dans les diapositives en faisant défiler la page verticalement. 

2. **Vue PDF :**
   *[Voir un exemple](https://revealjs.com/demo/?print-pdf)*

3. **Vue Diaporama :**
   [Voir un exemple](https://revealjs.com/demo/)
   Par défaut, reveal.js affiche les diapositives en mode diaporama, avec une navigation horizontale et verticale. Chaque section représente une diapositive, et les sous-sections permettent de créer des niveaux de navigation supplémentaires.
### Leur documentation
Pour essayer cet outil, vous pouvez [consulter leur documentation](https://revealjs.com), ou **suivre le tutoriel présenté ci-dessous.**
### Utilisation de slides.com

Pour ceux qui préfèrent une interface graphique pour créer leurs présentations reveal.js, slides.com est un éditeur en ligne développé par les mêmes créateurs que reveal.js. Il offre une interface conviviale pour concevoir des diapositives sans écrire de code.

**Avantages de slides.com :**
- **Interface intuitive :** Permet de créer des présentations en glisser-déposer.
- **Collaboration en temps réel :** Idéal pour les équipes travaillant sur des projets communs.
- **Publication facile :** Hébergez et partagez vos présentations directement depuis la plateforme.
### Tutoriel par Grafikart.fr
Nous avons trouvé ce tutoriel, très bien fait, pour vous initier à l'utilisation de cet outil auteur atypique.
<iframe width="560" height="315" src="https://www.youtube.com/embed/Xt-BH1E13HU?si=0Da8p3VQve1jPWMO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
### Conclusion

Reveal.js est une solution puissante et flexible pour créer des présentations web interactives. Que vous soyez développeur ou formateur, cette bibliothèque offre de nombreuses possibilités pour personnaliser et enrichir vos diaporamas. Pour ceux qui préfèrent une approche sans code, slides.com constitue une excellente alternative avec une interface graphique intuitive.

[^1]: https://grafikart.fr/tutoriels/reveal-js-presentation-2109
[^2]: https://www.youtube.com/watch?v=Xt-BH1E13HU
[^3]: https://github.com/hakimel/reveal.js
[^4]: https://github.com/hakimel/reveal.js/releases
[^5]: https://bookdown.org/yihui/rmarkdown/revealjs.html

---
*Rédigé avec l'aide de [Perplexity.ai](https://www.perplexity.ai/), prompté par Axelle Abbadie.*