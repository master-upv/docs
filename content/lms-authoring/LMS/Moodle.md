---
title: Moodle
draft: false
tags:
  - LMS
date: 2024-07-16
---
[Moodle](https://moodle.org/?lang=fr) (Modular Object-Oriented Dynamic Learning Environment) est l'un des LMS open-source les plus populaires au monde, largement utilisé dans l'enseignement supérieur et la formation professionnelle en France.

## Avantages de Moodle

1. **Open-source et gratuit** : Permet une grande flexibilité et des économies substantielles.
2. **Hautement personnalisable** : Grâce à son architecture modulaire et ses nombreux plugins.
3. **Communauté active** : Bénéficie d'un large soutien et de mises à jour régulières.
4. **Multilingue** : Disponible dans plus de 100 langues.
5. **Riche en fonctionnalités** : Offre une large gamme d'outils pédagogiques intégrés.
6. **Intégration facile** : Compatible avec de nombreux systèmes tiers.

## Inconvénients de Moodle

1. **Courbe d'apprentissage** : Peut être complexe pour les débutants.
2. **Ressources techniques** : Nécessite des compétences pour l'installation et la maintenance.
3. **Interface utilisateur** : Parfois jugée moins moderne que certains concurrents.
4. **Personnalisation avancée** : Peut nécessiter des compétences en développement.

## Rôles dans Moodle

Moodle propose plusieurs rôles prédéfinis, chacun avec des permissions spécifiques :

1. **Administrateur** : Contrôle total sur la plateforme.
2. **Gestionnaire** : Gère les cours et les utilisateurs.
3. **Créateur de cours** : Peut créer et gérer ses propres cours.
4. **Enseignant** : Peut enseigner et évaluer dans un cours.
5. **Enseignant non-éditeur** : Peut enseigner mais pas modifier le contenu du cours.
6. **Étudiant** : Peut accéder et participer aux cours.
7. **Invité** : Accès limité en lecture seule.


> [!info] Rôle de l'ingénieur pédagogique
> Les ingénieurs pédagogiques ne sont pas toujours administrateur de la plateforme sur laquelle ils interviennent.
> Ils peuvent être gestionnaires, créateurs ou enseignants s'ils sont là pour intervenir dans la production ou la modification d'un cours.
> Ou alors, simplement étudiant ou enseignant non-éditeur si leur rôle est de proposer un feedback à l'enseignant, ou accompagner l'enseignant à une modification en autonomie.


## Principe des plugins

Les plugins sont des modules complémentaires qui étendent les fonctionnalités de Moodle. Ils permettent d'ajouter de nouvelles activités, blocs, thèmes, ou intégrations. L'installation de plugins se fait généralement via l'interface d'administration de Moodle.

## Éditeur de contenus

L'éditeur de contenus de Moodle est basé sur TinyMCE et offre plusieurs fonctionnalités avancées :

- **Intégration Bootstrap** : Permet l'utilisation des classes Bootstrap pour une mise en page responsive.
- **Intégration multimédia** : Facilite l'ajout de vidéos, images, et autres médias.
- **Éditeur HTML** : Offre la possibilité d'éditer directement le code HTML pour plus de contrôle.
- **Plugins d'édition** : Des plugins comme Atto ou TinyMCE peuvent étendre les capacités de l'éditeur.


> [!tip] Hack Moodle
> Nous dédions un tag aux #hacks-moodle, car avec les intégrations sus-mentionnées, il est possible d'avoir des mises en pages très performantes en matière d'apprentissage ou de structuration des contenus.

## Plugins courants

1. **H5P** : Pour créer du contenu interactif.
2. **BigBlueButton** : Pour les visioconférences.
3. **Questionnaire** : Pour créer des sondages avancés.
4. **Certificate** : Pour générer des certificats personnalisés.
5. **Attendance** : Pour gérer les présences.

## Thèmes populaires

1. **Boost** : Le thème par défaut, moderne et responsive.
2. **Adaptable** : Très personnalisable et populaire.
3. **Moove** : Design épuré et fonctionnel.
4. **Essential** : Thème polyvalent avec de nombreuses options.
5. **Fordson** : Axé sur l'expérience utilisateur et la personnalisation.

En conclusion, Moodle offre une solution robuste et flexible pour la gestion de l'apprentissage en ligne. Sa nature open-source et sa grande communauté en font un choix privilégié pour de nombreuses institutions éducatives, malgré une certaine complexité initiale. La possibilité d'étendre ses fonctionnalités via des plugins et de personnaliser son apparence avec des thèmes en fait une plateforme adaptable à une grande variété de besoins pédagogiques.

---
*Rédigé avec l'aide de [Perplexity.ai](https://www.perplexity.ai/), prompté par Axelle Abbadie.*