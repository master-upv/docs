---
title: 00 Présentation
draft: true
tags: 
date: 2024-09-25
aliases:
---
La Causerie est un entretien enrichi, présenté comme un média interactif.
Elle est publiée connexe à [la revue LHUMAINE](https://lhumaine.numerev.com/), en tant que préambule.

*Seront publiés plus tard les éléments conceptuels.*

Ici, nous documentons le chantier pédagogique, pour permettre aux étudiants de notre master et d'autres masters de notre laboratoire de participer à la Causerie.

### Pré-requis
Pour l'instant, tous les outils que nous utilisons pour la Causerie sont sur MacOS. Nous travaillons à proposer une version Windows et/ou Linux.

- [ ] Enregistrement dans le cloud ZOOM
- [ ] Mindmap complétée pendant l'entretien
- [ ] Kit Causerie (*à venir*)
- [ ] Photo haute qualité de l'interviewé
- [ ] iMovie ([télécharger](https://apps.apple.com/fr/app/imovie/id408981434?mt=12))
- [ ] Keynote ([télécharger](https://apps.apple.com/fr/app/keynote/id409183694?mt=12))


> [!warning] Logiciels
> L'utilisation de **iMovie n'est pas obligatoire**, si vous êtes à l'aise avec un autre outil, vous pouvez l'utiliser. Cependant, veillez à consulter la méthode de marquage car elle vous sera utile.
> 
> En revanche, **Keynote est obligatoire**. Pour l'instant, le kit est conçu sur Keynote, car celui-ci permet un export HTML fonctionnel que l'on peut héberger facilement.

### Tutoriels
1. [[1-assembler|Assembler l'enregistrement]]
2. [[2-decoupage|Découpage thématique multi-média]]
%% 3. [[3-ajustement-kit|Ajustement Kit Causerie]]
4. [[4-curations|Curations complémentaires]]
5. [[5-integration|Intégration]]
6. [[6-deploiement|Déploiement]] %%