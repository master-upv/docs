---
title: 01 Assembler enregistrement
draft: true
tags: 
date: 2024-09-25
aliases:
---
# L'explication
Lorsque l'enregistrement est réalisé par Zoom, on enregistre dans le cloud.
Cet enregistrement permet d'obtenir la vidéo au son unifié, et chaque intervenant sur un fichier audio unique.

À partir de ceci, et iMovie, on construit un projet avec une piste image (vidéo) et autant de pistes audio qu'il y a d'intervenants.

# La procédure
1. Nouveau film (`⌘` + `N`)
2. Glissez-déposez l'ensemble de vos fichiers au début de la séquence

![[causerie-step1.gif]]

Cette nouvelle séquence nous permettra d'en faire de nouvelles à exporter soit en vidéo, soit en audio, lors de l'étape suivante : [[2-decoupage|le découpage thématique]].