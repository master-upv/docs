---
title: 02 Découpage thématique
draft: true
tags: 
date: 2024-09-25
aliases:
---
# L'explication
Vous devez sélectionner les moment où l'interviewé parle d'un des éléments abordés dans la Causerie.

Le découpage thématique est réalisé à partir d'une écoute attentive et de la mindmap de l'entretien.

![[causerie-mindmap-vierge.pdf]]

La mindmap est réalisée de façon à vous aiguiller sur la façon de transmettre un élément abordé au cours de l'entretien dans le média interactif.

Voici, en exemple, celle complétée pour l'entretien de T. Huchon : 
![[causerie-mindmap-huchon.pdf]]


# La procédure

1. Écoute naïve pour marquer et identifier les séquences thématiques
2. Affiner le marquage pour sélectionner les séquences à prélever.
3. Découpage et export pour travailler par thème ensuite.

## Première écoute, marquage naïf et lien thématique
Sur votre projet où toutes vos pistes sont assemblées, vous allez parcourir la vidéo et marquer avec la touche `M` les débuts de chaque sujet.

Sur une note (numérique ou papier), notez les marquages que vous faites, et annotez les d'une brève explication.

Le plus pratique reste de codifier, au début vous mettrez `01:03 = Question Trump élection`. Puis, vous affinerez en choisissant précisément la séquence que vous voudrez conserver.

Pour l'instant, vous vous concentrez à identifier de potentielles séquences à prélever.

> [!hint] Lier les marqueurs à la mindmap !
> Dès la première écoute, en commentaires ou directement sur votre document, liez les séquences potentielles avec les concepts présentés dans la map.
> Pour cela, vous pouvez commencer à nommer les séquences potentielles. Dans mon cas d'exemple, T. Huchon parle avec L. Alidières de la possible réélection de Trump, donc je nomme ma séquence `TRUMP`.

À la fin de cette première écoute, vous devriez avoir retrouvé et naïvement marqué toutes les notes prises sur la mindap de l'entretien. Si ce n'est pas le cas, démarrez une nouvelle écoute naïve. Habituez-vous à l'écoute, vous allez probablement écouter cet enregistrement une bonne vingtaine de fois.
## Affiner le marquage, coder les séquences

Dès que je vais découper cette séquence et la prélever de mon projet, il est possible que le timer ne soit plus le bon. Donc, codifiez vos marqueurs avec un nom unique, et une séquence à prélever est constituée d'un marqueur de début et d'un marqueur de fin.

Voici un exemple de marqueurs codifiés : 
- 01:03 TRUMP1 = Question trump élection
- 01:46 { TRUMP2 = Début chance Trump gagner élections
- 02:45 } TRUMP3 = Fin chance Trump gagner élections

> [!success] Pourquoi cette codification est recommandée ?
> Voici sa syntaxe : `timer + accolade d'ouverture ou fermeture`  `Séquence + numérotation`= `annotation` 
> Ainsi, la séquence ici s'appelle TRUMP, elle commence au marqueur unique TRUMP2 et termine au marquer unique TRUMP3.

Vous allez mettre beaucoup de marqueurs, croyez-moi, donc un code lisible vous facilitera la prise de décision.

## Découpage et export des séquences

Vous avez identifié toutes vos séquences, et chaque marqueur est à la fois présent sur votre projet iMovie et sur votre document.

Sélectionnez l'ensemble des pistes de votre séquence, positionnez votre curseur sur le marqueur d'ouverture et appuyez sur les touches `⌘` + `B`.

Faites en de même avec le marqueur de fermeture de la séquence, puis vérifiez votre séquence avec `clic-droit` + `Lecture`.

Si vous avez fait une erreur, `⌘` + `Z` vous permet d'annuler votre dernière action.

![[causerie-step2-1.gif]]

Une fois votre plan scindé, vous pouvez `clic-droit` + `Afficher dans les données du projet`.
![[causerie-step2-2.png]]

Alors, dans les données du projet, plus haut, vous verrez une zone sélectionnée : 
![[causerie-step2-3.png]]

Avec `clic-droit` + `Favoris` vous signalez à iMovie que c'est cette partie que vous souhaitez exporter.

![[causerie-step2-4.png]]

Vous pouvez ensuite exporter grâce à `Fichier` → `Partage` → `Fichier...` 
Vérifiez que la durée est cohérente, nommez votre fichier puis enregistrez-le.

Ensuite, vous devrez "Déclasser" votre séquence, dans les données du projet, `clic-droit` sur la zone sélectionnée, `Déclasser`.

Passez ensuite à la séquence suivante, en répétant la procédure.