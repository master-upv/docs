---
title: "Les outils de conception : des alliés pour la créativité et la productivité"
draft: false
tags: 
date: 2024-07-16
---
Nous vous proposons une rapide revue d'outils qui peuvent vous accompagner durant le processus de conception. Cette page ne traite pas de l'utilisation de l'[[iag-conception-assist|IA générative comme assistant dans la conception]].

### Outils de pensée visuelle

**Miro**
- Tableau blanc collaboratif en ligne
- Idéal pour le brainstorming, la cartographie mentale et la création de diagrammes

**Alternative open source : Frama.space**
- Outil de tableau blanc collaboratif développé par Framasoft
- Fonctionnalités similaires à Miro, avec un accent sur la protection des données

### Outils de télécollaboration

**Microsoft Teams**
- Plateforme de communication intégrée
- Chat, appels vidéo, partage de fichiers et intégration d'applications

**Slack**
- Messagerie instantanée pour équipes
- Canaux de discussion organisés par thèmes ou projets

**Discord**
- Plateforme de communication initialement conçue pour les gamers, mais de plus en plus utilisée pour la collaboration professionnelle
- Serveurs personnalisables, canaux de discussion textuels et vocaux, partage d'écran

**Alternative open source : Mattermost**
- Plateforme de collaboration open source
- Fonctionnalités similaires à Slack, avec la possibilité d'auto-hébergement

### Outils de gestion de projet

**Trello**
- Interface intuitive basée sur des tableaux, listes et cartes
- Méthode Kanban pour visualiser le flux de travail

**ClickUp**
- Outil tout-en-un pour la gestion de projet et la productivité
- Multiples vues (liste, tableau, calendrier, Gantt, etc.)

**Alternative open source : Framaboard**
- Outil de gestion de projet basé sur Kanban
- Développé par Framasoft, respectueux de la vie privée

### Outils d'idéation

**Mural**
- Espace de travail visuel pour la collaboration créative
- Idéal pour les sessions de brainstorming et les ateliers de design thinking

**Alternative open source : Focalboard**
- Outil de gestion de projet et d'idéation open source
- Peut être utilisé comme alternative à Trello ou Notion

## Conclusion

Ces outils de conception, qu'ils soient propriétaires ou open source, offrent une variété de solutions pour améliorer la collaboration, la gestion de projet et la créativité au sein des équipes. Les alternatives open source comme celles proposées par Framasoft ou d'autres communautés offrent des fonctionnalités similaires tout en mettant l'accent sur la protection des données et la liberté d'utilisation.

Il est important de choisir les outils qui correspondent le mieux à vos besoins spécifiques, à la culture de votre équipe et à vos préoccupations en matière de confidentialité et de contrôle des données. N'hésitez pas à tester différentes solutions, propriétaires et open source, pour trouver celles qui s'intègrent le mieux à votre flux de travail.

Citations:
1. https://bop.fipf.org/projets-de-classe-en-telecollaboration/
2. https://forums.scenari.org/c/modeles-documentaires/6
3. https://journals.openedition.org/alsic/3087
4. https://www.innovation-pedagogique.fr/article532.html
5. https://journals.openedition.org/alsic/3128

---
*Rédigé avec l'aide de [Perplexity.ai](https://www.perplexity.ai/), prompté par Axelle Abbadie.*