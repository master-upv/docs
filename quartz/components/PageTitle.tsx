import { pathToRoot } from "../util/path"
import { QuartzComponent, QuartzComponentConstructor, QuartzComponentProps } from "./types"
import { classNames } from "../util/lang"
import { i18n } from "../i18n"

const PageTitle: QuartzComponent = ({ fileData, cfg, displayClass }: QuartzComponentProps) => {
  const title = cfg?.pageTitle ?? i18n(cfg.locale).propertyDefaults.title
  const subtitle = cfg?.pageSubTitle ?? i18n(cfg.locale).propertyDefaults.subtitle
  const parentUrl = cfg?.parentUrl
  const baseDir = pathToRoot(fileData.slug!)
  return (
    <div class="header-custom">
        <a title="Retour sur le site du master" href={parentUrl}>
            <div class={classNames("page-logo")}><img src={`${baseDir}/static/icon.png`} alt="" /></div>
        </a>
        <a href={baseDir}>
        <h1 class={classNames(displayClass, "page-title")}>
          {title}
        </h1>
        <h2 class={classNames("page-subtitle")}>{subtitle}</h2>
        </a>
    </div>
  )
}

PageTitle.css = `
.page-title {
  margin: 0;
}
.header-custom {
    text-align: center;
}
h2.page-subtitle {
    font-weight: 200;
    margin: 5px;
}
div.page-logo > img {
    max-height: 100px;
}
`

export default (() => PageTitle) satisfies QuartzComponentConstructor
